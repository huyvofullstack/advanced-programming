// Fail.cpp : implementation file
//

#include "stdafx.h"
#include "AP.h"
#include "Fail.h"
#include "afxdialogex.h"


// Fail dialog

IMPLEMENT_DYNAMIC(Fail, CDialogEx)

Fail::Fail(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG2, pParent)
{

}

Fail::~Fail()
{
}

void Fail::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_dd, pd);
	CBitmap dd;
	dd.LoadBitmapW(IDB_BITMAP9);
	pd.SetBitmap((HBITMAP)dd.Detach());
}


BEGIN_MESSAGE_MAP(Fail, CDialogEx)
END_MESSAGE_MAP()


// Fail message handlers
