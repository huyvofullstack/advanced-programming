// Grandma.cpp : implementation file
//

#include "stdafx.h"
#include "AP.h"
#include "Grandma.h"
#include "afxdialogex.h"


// Grandma dialog

IMPLEMENT_DYNAMIC(Grandma, CDialogEx)

Grandma::Grandma(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{

}

Grandma::~Grandma()
{
}

void Grandma::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_g, pg);
	CBitmap grandma;
	grandma.LoadBitmapW(IDB_BITMAP8);
	pg.SetBitmap((HBITMAP)grandma.Detach());
}


BEGIN_MESSAGE_MAP(Grandma, CDialogEx)
END_MESSAGE_MAP()


// Grandma message handlers
