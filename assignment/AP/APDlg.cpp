
// APDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AP.h"
#include "APDlg.h"
#include "afxdialogex.h"
#include "MyDialog.h"
#include <iostream>
#include <fstream>
#include "Grandma.h"
#include "Fail.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAPDlg dialog



CAPDlg::CAPDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_MAP, pParent)
{
	MyDialog mydlg = new MyDialog();
	mydlg.DoModal();
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_1, p0);
	DDX_Control(pDX, IDC_2, p1);
	DDX_Control(pDX, IDC_3, p2);
	DDX_Control(pDX, IDC_4, p3);
	DDX_Control(pDX, IDC_5, p4);
	DDX_Control(pDX, IDC_6, p5);
	DDX_Control(pDX, IDC_7, p6);
	DDX_Control(pDX, IDC_8, p7);
	DDX_Control(pDX, IDC_9, p8);
	DDX_Control(pDX, IDC_10, p9);
	DDX_Control(pDX, IDC_11, p10);
	DDX_Control(pDX, IDC_12, p11);
	DDX_Control(pDX, IDC_13, p12);
	DDX_Control(pDX, IDC_14, p13);
	DDX_Control(pDX, IDC_15, p14);
	DDX_Control(pDX, IDC_16, p15);
	DDX_Control(pDX, IDC_17, p16);
	DDX_Control(pDX, IDC_18, p17);
	DDX_Control(pDX, IDC_19, p18);
	DDX_Control(pDX, IDC_20, p19);
	DDX_Control(pDX, IDC_21, p20);
	DDX_Control(pDX, IDC_22, p21);
	DDX_Control(pDX, IDC_23, p22);
	DDX_Control(pDX, IDC_24, p23);
	DDX_Control(pDX, IDC_25, p24);
	DDX_Control(pDX, IDC_26, p25);
	DDX_Control(pDX, IDC_27, p26);
	DDX_Control(pDX, IDC_28, p27);
	DDX_Control(pDX, IDC_29, p28);
	DDX_Control(pDX, IDC_30, p29);
	DDX_Control(pDX, IDC_31, p30);
	DDX_Control(pDX, IDC_32, p31);
	DDX_Control(pDX, IDC_33, p32);
	DDX_Control(pDX, IDC_34, p33);
	DDX_Control(pDX, IDC_35, p34);
	DDX_Control(pDX, IDC_36, p35);
	DDX_Control(pDX, IDC_37, p36);
	DDX_Control(pDX, IDC_38, p37);
	DDX_Control(pDX, IDC_39, p38);
	DDX_Control(pDX, IDC_40, p39);
	DDX_Control(pDX, IDC_41, p40);
	DDX_Control(pDX, IDC_42, p41);
	DDX_Control(pDX, IDC_43, p42);
	DDX_Control(pDX, IDC_44, p43);
	DDX_Control(pDX, IDC_45, p44);
	DDX_Control(pDX, IDC_46, p45);
	DDX_Control(pDX, IDC_47, p46);
	DDX_Control(pDX, IDC_48, p47);
	DDX_Control(pDX, IDC_49, p48);
	DDX_Control(pDX, IDC_50, p49);
	DDX_Control(pDX, IDC_51, p50);
	DDX_Control(pDX, IDC_52, p51);
	DDX_Control(pDX, IDC_53, p52);
	DDX_Control(pDX, IDC_54, p53);
	DDX_Control(pDX, IDC_55, p54);
	DDX_Control(pDX, IDC_56, p55);
	DDX_Control(pDX, IDC_57, p56);
	DDX_Control(pDX, IDC_58, p57);
	DDX_Control(pDX, IDC_59, p58);
	DDX_Control(pDX, IDC_60, p59);
	DDX_Control(pDX, IDC_61, p60);
	DDX_Control(pDX, IDC_62, p61);
	DDX_Control(pDX, IDC_63, p62);
	DDX_Control(pDX, IDC_64, p63);
	DDX_Control(pDX, IDC_65, p64);
	DDX_Control(pDX, IDC_66, p65);
	DDX_Control(pDX, IDC_67, p66);
	DDX_Control(pDX, IDC_68, p67);
	DDX_Control(pDX, IDC_69, p68);
	DDX_Control(pDX, IDC_70, p69);
	DDX_Control(pDX, IDC_71, p70);
	DDX_Control(pDX, IDC_72, p71);
	DDX_Control(pDX, IDC_73, p72);
	DDX_Control(pDX, IDC_74, p73);
	DDX_Control(pDX, IDC_75, p74);
	DDX_Control(pDX, IDC_76, p75);
	DDX_Control(pDX, IDC_77, p76);
	DDX_Control(pDX, IDC_78, p77);
	DDX_Control(pDX, IDC_79, p78);
	DDX_Control(pDX, IDC_80, p79);
	DDX_Control(pDX, IDC_81, p80);
}

BEGIN_MESSAGE_MAP(CAPDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CAPDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CAPDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CAPDlg message handlers

BOOL CAPDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CAPDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAPDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAPDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

bool CAPDlg::readMap() {
	fstream f("MUSHROOM.INP", ios::in);
	if (!f) {
		return false;
	}
	f >> r;
	f >> c;
	array = new int*[r];
	for (int i = 0; i < r; i++) {
		array[i] = new int[c];
	}
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			f >> array[i][j];
		}
	}
	f.close();

	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			switch (array[i][j]) {
			case 0:
				if (i == 0 && j == 0) {
					cbitmapbegin.LoadBitmapW(IDB_BITMAP1);
					map[i][j]->SetBitmap((HBITMAP)cbitmapbegin.Detach());
				}
				else if (i == 8 && j == 8) {
					cbitmapend.LoadBitmapW(IDB_BITMAP3);
					map[i][j]->SetBitmap((HBITMAP)cbitmapend.Detach());
				}
				break;
			case -1:
				cbitmap4.LoadBitmapW(IDB_BITMAP7);
				map[i][j]->SetBitmap((HBITMAP)cbitmap4.Detach());
				break;
			case 1:
				cbitmap1.LoadBitmapW(IDB_BITMAP4);
				map[i][j]->SetBitmap((HBITMAP)cbitmap1.Detach());
				break;
			case 2:
				cbitmap2.LoadBitmapW(IDB_BITMAP5);
				map[i][j]->SetBitmap((HBITMAP)cbitmap2.Detach());
				break;
			case 3:
				cbitmap3.LoadBitmapW(IDB_BITMAP6);
				map[i][j]->SetBitmap((HBITMAP)cbitmap3.Detach());
				break;
			}
		}
	}
	return true;
}

bool CAPDlg::backTrack(int i, int j, int numberOfMushroom, int lastMushroom) {
	if (i == 8 && j == 8 && numberOfMushroom >= 2) {
		return true;
	}
	for (int turn = 0; turn < 2; turn++) {
		if (turn == 0 && i + 1 < 9 && array[i + 1][j] != -1) {
			if (lastMushroom != array[i + 1][j] && array[i][j + 1] != 0) {
				numberOfMushroom++;
			}
			lastMushroom = array[i + 1][j];
			if (backTrack(i + 1, j, numberOfMushroom, lastMushroom)) {
				cbitmapbegin.LoadBitmapW(IDB_BITMAP1);
				map[i + 1][j]->SetBitmap((HBITMAP)cbitmapbegin.Detach());
				return true;
			}
		}
		else if (turn == 1 && j + 1 < 9 && array[i][j + 1] != -1) {
			if (lastMushroom != array[i][j + 1] && array[i][j + 1] != 0) {
				numberOfMushroom++;
			}
			lastMushroom = array[i][j + 1];
			if (backTrack(i, j + 1, numberOfMushroom, lastMushroom)) {
				cbitmapbegin.LoadBitmapW(IDB_BITMAP1);
				map[i][j + 1]->SetBitmap((HBITMAP)cbitmapbegin.Detach());
				return true;
			}
		}
	}
	return false;
}

void CAPDlg::OnBnClickedOk()
{
	backTrack(0, 0, numberOfMushroom, lastMushroom);
	if (backTrack(0, 0, numberOfMushroom, lastMushroom)) {
		Grandma grandma = new Grandma();
		grandma.DoModal();
	}
	else {
		Fail fail = new Fail();
		fail.DoModal();
	}
}


void CAPDlg::OnBnClickedButton1()
{
	readMap();
	GetDlgItem(IDOK)->EnableWindow(true);
}
