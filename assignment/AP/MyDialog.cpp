// MyDialog.cpp : implementation file
//

#include "stdafx.h"
#include "AP.h"
#include "MyDialog.h"
#include "afxdialogex.h"
#include "mmsystem.h"


// MyDialog dialog

IMPLEMENT_DYNAMIC(MyDialog, CDialogEx)

MyDialog::MyDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_INFO, pParent)
{

}

MyDialog::~MyDialog()
{
	PlaySound(_T("02_Orchestral_Suite_No.wav"),
		NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
}

void MyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(MyDialog, CDialogEx)
	ON_BN_CLICKED(IDOK, &MyDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// MyDialog message handlers


void MyDialog::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}
