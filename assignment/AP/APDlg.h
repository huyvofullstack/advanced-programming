
// APDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CAPDlg dialog
class CAPDlg : public CDialogEx
{
// Construction
public:
	CAPDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_AP_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	bool readMap();
	bool backTrack(int i, int j, int numberOfMushroom, int lastMushroom);
	DECLARE_MESSAGE_MAP()
public:
	CStatic p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18,
		p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37,
		p38, p39, p40, p41, p42, p43, p44, p45, p46, p47, p48, p49, p50, p51, p52, p53, p54, p55, p56,
		p57, p58, p59, p60, p61, p62, p63, p64, p65, p66, p67, p68, p69, p70, p71, p72, p73, p74, p75,
		p76, p77, p78, p79, p80;

	CStatic* map[9][9] = { {&p0, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8}, {&p9, &p10, &p11, &p12, &p13, &p14, &p15, &p16, &p17}, {&p18,
	&p19, &p20, &p21, &p22, &p23, &p24, &p25, &p26}, {&p27, &p28, &p29, &p30, &p31, &p32, &p33, &p34, &p35}, {&p36, &p37,
	&p38, &p39, &p40, &p41, &p42, &p43, &p44}, {&p45, &p46, &p47, &p48, &p49, &p50, &p51, &p52, &p53}, {&p54, &p55, &p56,
		&p57, &p58, &p59, &p60, &p61, &p62}, {&p63, &p64, &p65, &p66, &p67, &p68, &p69, &p70, &p71}, {&p72, &p73, &p74, &p75,
		&p76, &p77, &p78, &p79, &p80} };

	CBitmap cbitmap1, cbitmap2, cbitmap3, cbitmap4, cbitmapbegin, cbitmapend;
	int **array, r, c;
	int numberOfMushroom = 0, lastMushroom = 0;

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
};
