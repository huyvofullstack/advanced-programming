#pragma once
#include "afxwin.h"


// Fail dialog

class Fail : public CDialogEx
{
	DECLARE_DYNAMIC(Fail)

public:
	Fail(CWnd* pParent = NULL);   // standard constructor
	virtual ~Fail();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CStatic pd;
};
