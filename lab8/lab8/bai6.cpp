#include <iostream>
#include <string>
using namespace std;

void changeToInt(char s[]) {
	double number = (s[0] - 0x30);
	for (int i = 1; i < strlen(s); i++) {
		number = number * 10 + (s[i] - '0'); // chuyen chuoi so nguyen thanh so nguyen
	}
	cout << number << endl;
}

int main() {
	char s[] = "123456";
	changeToInt(s);
	return 0;
}