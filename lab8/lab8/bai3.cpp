#include <iostream>
#include <string>
using namespace std;

void check(char a[], int n) {
	int flag = 0;
	for (int i = 0; i < n / 2; i++) {
		if (a[i] != a[n - 1 - i]) {
			flag++;
		}
	}
	if (flag == 0) {
		cout << "Chuoi doi xung" << endl;
	}
	else {
		cout << "Chuoi khong doi xung" << endl;
	}
}

int main() {
	char s[] = "bbaabb";
	check(s, strlen(s));
	return 0;
}