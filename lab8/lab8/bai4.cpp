#include <iostream>
#include <string>
using namespace std;

int check(char s[], int n) {
	int flag = 0;
	for (int i = 0; i < n; i++) {
		if (s[i] >= '0' && s[i] <= '9' && s[i] || s[i] == '.' || s[i] == '/') {
			flag++;
		}
	}
	if (flag == n) {
		return 1;
	}
	else return 0;
}

int main() {
	char s[] = "12/45";
	cout << check(s, strlen(s)) << endl;
	return 0;
}