#include <iostream>
#include <string>
using namespace std;

int main() {
	string a, b = "12,23,34,45";
	for (int i = 0; i < b.length(); i++) {
		if ((b[i] <= '9' && b[i] >= '0') || (b[i] <= 'z' && b[i] >= 'a') || (b[i] <= 'Z' && b[i] >= 'A')) {
			a += b[i];
		}
		else {
			cout << a << endl;
			a.clear();
		}
	}
	cout << a << endl;
	return 0;
}