#include <iostream>
#include <string>
#include <fstream>
using namespace std;

char check(char x) {
	if (x >= 'a' && x <= 'z') {
		x = x - 32;
	}
	return x;
}

void coppyupper(char* filename1, char* filename2) {
	string a;
	fstream f(filename1, ios::in);
	fstream f2(filename2, ios::out);
	while (!f.eof()) {
		a += check(f.get());
	}
	a[a.length() - 1] = '\n';
	f2 << a;
	f.close();
	f2.close();
}

int main() {
	coppyupper("first.txt", "second.txt");
	return 0;
}