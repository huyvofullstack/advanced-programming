#include <iostream>
#include <string>
using namespace std;

void reverse(char s[]) {
	char temp;
	for (int i = 0; i < strlen(s) / 2; i++) {
		temp = s[i];
		s[i] = s[strlen(s) - 1 - i];
		s[strlen(s) - 1 - i] = temp;
	}
}

int main() {
	char s[] = "qwerty";
	reverse(s);
	cout << s << endl;
	return 0;
}