#include <iostream>
#include <string>
#include <fstream>
using namespace std;

char check(char x, int &count) {
	if (x >= 'a' && x <= 'z' || x >= 'A' && x <= 'Z') {
		count++;
	}
	else {
		return NULL;
	}
	return x;
}

void dem(char* filename) {
	fstream f(filename, ios::in);
	string a;
	int count = 0;
	while (!f.eof()) {
		a += check(f.get(), count);
	}
	a[a.length() - 1] = '\0';
	f.close();
	cout << a << " " << count << endl;
}

int main() {
	dem("OUT.txt");
	return 0;
}