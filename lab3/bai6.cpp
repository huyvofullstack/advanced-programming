#include <iostream>
#include <time.h>
using namespace std;

void createMatrix(int** &ptr, int row, int* &column) {
	ptr = new int*[row];
	for (int i = 0; i < row; i++) {
		column[i] = rand() % 10 + 1;
		ptr[i] = new int[column[i]];
		for (int j = 0; j < column[i]; j++) {
			ptr[i][j] = (rand() % 100 + 1);
		}
	}
}

void showValue(int** &ptr, int row, int* column) {
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < column[i]; j++) {
			cout << ptr[i][j] << " ";
		}
		cout << endl;
	}
}

int findMaxRow(int** &ptr, int row, int* column) {
	int *maxPtr = new int[row];
	int result = 0;
	for (int i = 0; i < row; i++) {
		int max = 0;
		for (int j = 0; j < column[i]; j++) {
			max += ptr[i][j];
		}
		maxPtr[i] = max;
	}
	int max = maxPtr[0];
	for (int i = 0; i < row; i++) {
		if (max < maxPtr[i]) {
			max = maxPtr[i];
			result = i;
		}
	}
	return result;
}

int main() {
	srand(time(0));
	int row = (rand() % 10 + 1);
	int *column = new int[row];
	int** ptr = NULL;
	createMatrix(ptr, row, column);
	showValue(ptr, row, column);
	cout << endl << "Hang co tong phan tu lon nhat la hang " << findMaxRow(ptr, row, column) << endl;
	for (int i = 0; i < row; i++) {
		delete[] ptr[i];
	}
	delete[] ptr;
	return 0;
}