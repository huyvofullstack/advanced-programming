#include <iostream>
#include <string>
using namespace std;

class Computer {
	char* chiptype;
	int speed;
public:
	Computer() {
		chiptype = NULL;
		speed = 0;
	}

	Computer(char* chiptype, int speed) {
		this->chiptype = new char[strlen(chiptype) + 1];
		strcpy(this->chiptype, chiptype);//sao chep noi dung chiptype sang this->chiptype;
		this->speed = speed;
	};

	Computer(Computer &cp) {
		this->chiptype = new char[strlen(cp.chiptype) + 1];
		strcpy(this->chiptype, cp.chiptype);
		this->speed = cp.speed;
	}

	~Computer() {
		if (this->chiptype) {
			delete []chiptype;
		}
	}

	void getdetails() {
		chiptype = new char[256];
		cout << "Chip: ";
		gets(chiptype);
		cout << "Speed: ";
		cin >> speed;
		cin.sync();
	}

	void showdetails() {
		cout << "Chip: " << chiptype << " " << "Speed: " << speed << endl;
	}
};

int main() {
	Computer cp1;
	Computer cp2("Huy", 20);
	Computer cp3(cp2);
	cp1.getdetails();
	cp2.getdetails();
	cp3.getdetails();
	cp1.showdetails();
	cp2.showdetails();
	cp3.showdetails();
	return 0;
}