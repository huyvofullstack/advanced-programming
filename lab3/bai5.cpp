#include <iostream>
using namespace std;

class Matrix {
	int row;
	int column;
	int** ptr;
public:
	Matrix() {
		row = 0;
		column = 0;
		ptr = NULL;
	}

	Matrix(int row, int column) {
		this->row = row;
		this->column = column;
		this->ptr = new int*[row + 1];
		for (int i = 0; i < column; i++) {
			this->ptr[i] = new int[column];
		}
	}

	void enterMatrix() {
		for (int i = 0; i < this->row; i++) {
			for (int j = 0; j < this->column; j++) {
				cout << "matrix[" << i << "][" << j << "]: ";
				cin >> this->ptr[i][j];
			}
		}
	}

	void showMatrix() {
		for (int i = 0; i < this->row; i++) {
			for (int j = 0; j < this->column; j++) {
				cout << this->ptr[i][j] << " ";
			}
			cout << endl;
		}
	}

	~Matrix() {
		if (this->ptr) {
			for (int i = 0; i < this->column; i++) {
				delete[] this->ptr[i];
			}
			delete[] this->ptr;
			this->ptr = NULL;
		}
	}
};

int main() {
	Matrix x(3, 4);
	x.enterMatrix();
	x.showMatrix();
	return 0;
}