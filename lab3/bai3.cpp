#include <iostream>
#include <fstream>
using namespace std;

class Array1D {
	int n;
	int *a;
public:
	Array1D() {
		n = 0;
		a = NULL;
	}

	Array1D(int n, int *a) {
		this->n = n;
		this->a = new int[n];
		for (int i = 0; i < n; i++) {
			this->a[i] = a[i];
		}
	}

	Array1D(Array1D &array) {
		this->n = array.n;
		this->a = new int[array.n];
		for (int i = 0; i < array.n; i++) {
			this->a[i] = array.a[i];
		}
	}
	
	int readArray1D(char* filename) {
		fstream f(filename, ios::in);
		if (!f) {
			cout << "Khong mo duoc file" << endl;
			return 0;
		}
		f >> this->n;
		this->a = new int[this->n];
		int i = 0;
		while (!f.eof()) {
			f >> a[i];
			i++;
		}
		f.close();
		return 1;
	}

	int writeArray1D(char* filename) {
		fstream f(filename, ios::out);
		if (!f) {
			cout << "Khong mo duoc file" << endl;
			return 0;
		}
		f << this->n << endl;
		for (int i = 0; i < this->n; i++) {
			f << this->a[i] << " ";
		}
		f.close();
		return 1;
	}

	void show() {
		for (int i = 0; i < this->n; i++) {
			cout << this->a[i] << " ";
		}
		cout << endl;
	}

	int timMax() {
		int max = 0;
		for (int i = 0; i < this->n; i++) {
			if (max < this->a[i]) {
				max = this->a[i];
			}
		}
		return max;
	}

	float tinhTrungBinh() {
		float trungBinh = 0;
		for (int i = 0; i < this->n; i++) {
			trungBinh += this->a[i];
		}
		return (trungBinh / this->n);
	}

	void Concat(Array1D a, Array1D b) {
		this->n = a.n + b.n;
		this->a = new int[this->n];
		int i = 0;
		for (int j = 0; j < a.n; j++) {
			this->a[i] = a.a[j];
			i++;
		}
		for (int j = 0; j < b.n; j++) {
			this->a[i] = b.a[j];
			i++;
		}
	}

	bool deleteX(int m) {
		int temp = n;
		for (int i = 0; i < this->n; i++) {
			if (m == this->a[i]) {
				for (int k = i; k < this->a[i] - 1; k++) {
					swap(this->a[i], this->a[i + 1]);
				}
				n--;
			}
		}
		if (n < temp) {
			return true;
		}
		else {
			return false;
		}
	}

	~Array1D() {
		if (this->a) {
			delete[] a;
		}
	}
};

int main() {
	Array1D a;
	a.readArray1D("input.txt");
	a.show();
	a.writeArray1D("output.txt");
	//Array1D b;
	//b.readArray1D("input.txt");
	//a.Concat(a, b);
	//cout << a.deleteX(1) << endl;;
	//cout << a.timMax() << endl;
	//cout << a.tinhTrungBinh() << endl;
	return 0;
}