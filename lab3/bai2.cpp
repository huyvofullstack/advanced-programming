#include <iostream>
using namespace std;

class School {
	int numbersOfStudent;
	char** studentNames;
	int* studentGrades;
public:
	School() {
		numbersOfStudent = 0;
		studentNames = NULL;
		studentGrades = NULL;
	}

	School(int numbersOfStudent, char** studentNames, int* studentGrades) {
		this->numbersOfStudent = numbersOfStudent;
		this->studentGrades = new int[numbersOfStudent];
		this->studentNames = new char*[numbersOfStudent];
		for (int i = 0; i < numbersOfStudent; i++) {
			this->studentNames[i] = new char[strlen(studentNames[i]) + 1];
			strcpy(this->studentNames[i], studentNames[i]);
			this->studentGrades[i] = studentGrades[i];
		}
	}

	School(School &x) {
		this->numbersOfStudent = x.numbersOfStudent;
		this->studentGrades = new int[x.numbersOfStudent];
		this->studentNames = new char*[x.numbersOfStudent];
		for (int i = 0; i < x.numbersOfStudent; i++) {
			this->studentNames[i] = new char[strlen(x.studentNames[i]) + 1];
			strcpy(this->studentNames[i], x.studentNames[i]);
			this->studentGrades[i] = studentGrades[i];
		}
	}

	~School() {
		if (studentNames) {
			for (int i = 0; i < numbersOfStudent; i++) {
				delete[] studentNames[i];
			}
			delete[] studentNames;
			studentNames = NULL;
		}
		if (studentGrades) {
			delete[] studentGrades;
			studentGrades = NULL;
		}
	}

	void showInfo() {
		for (int i = 0; i < numbersOfStudent; i++) {
			cout << "Ten hs thu " << (i + 1) << ": " << this->studentNames[i] << endl;
			cout << "Diem hs thu " << (i + 1) << ": " << this->studentGrades[i] << endl;
		}
	}
};

int main() {
	char** nameOfStudents = new char*[3];
	for (int i = 0; i < 3; i++) {
		nameOfStudents[i] = new char[256];
		gets(nameOfStudents[i]);
	}
	int studentMarks[3] = { 7, 8, 9 };
	School x(3, nameOfStudents, studentMarks);
	x.showInfo();
	return 0;
}