﻿#include <iostream>
#include <fstream>
using namespace std;

class Matrix {
private:
	int r;
	int c;
	int **a;
public:
	Matrix() {
		r = 0;
		c = 0;
		a = NULL;
	}

	Matrix(int r, int c, int **a) {
		this->r = r;
		this->c = c;
		this->a = new int*[r];
		for (int i = 0; i < r; i++) {
			a[i] = new int[c];
		}
	}

	Matrix(Matrix &x) {
		this->r = x.r;
		this->c = x.c;
		this->a = new int*[x.r];
		for (int i = 0; i < x.r; i++) {
			a[i] = new int[x.c];
		}
	}

	~Matrix() {
		if (a) {
			for (int i = 0; i < r; i++) {
				delete[] a[i];
			}
			delete[] a;
			a = NULL;
		}
	}

	int readMatrix(char* filename) {
		fstream f(filename, ios::in);
		if (!f) {
			cout << "Khong mo duoc file" << endl;
			return 0;
		}
		f >> this->r;
		f >> this->c;
		this->a = new int*[this->r];
		while (!f.eof()) {
			for (int i = 0; i < this->r; i++) {
				a[i] = new int[this->c];
				for (int j = 0; j < this->c; j++) {
					f >> a[i][j];
				}
			}
		}
		f.close();
		return 1;
	}

	int writeMatrix(char* filename) {
		fstream f(filename, ios::out);
		if (!f) {
			cout << "Khong mo duoc file" << endl;
			return 0;
		}
		f << this->r << " " << this->c << endl;
		for (int i = 0; i < this->r; i++) {
			for (int j = 0; j < this->c; j++) {
				f << a[i][j] << " ";
			}
			f << endl;
		}
		f.close();
		return 1;
	}

	void showMatrix() {
		for (int i = 0; i < this->r; i++) {
			for (int j = 0; j < this->c; j++) {
				cout << a[i][j] << " ";
			}
			cout << endl;
		}
	}

	void transpose() {
		for (int i = 0; i < this->c; i++) {
			for (int j = 0; j < this->r; j++) {
				cout << a[j][i] << " ";
			}
			cout << endl;
		}
	}

	void increaseColumn(int k) {
		for (int i = 0; i < this->c - 1; i++) {
			for (int j = i + 1; j < this->c; j++) {
				if (a[k][i] > a[k][j]) {
					swap(a[k][i], a[k][j]);
				}
			}
		}
	}

	void increaseRow() {
		for (int i = 0; i < this->r; i++) {
			increaseColumn(i);
		}
	}

	void sumMatrix(Matrix a, Matrix b) {
		if (a.c != b.r) {
			cout << "Hai ma tran khong kha tich" << endl;
		}
		else {
			//mind blown
		}
	}

};

int main() {
	Matrix a;
	Matrix b;
	b.readMatrix("ex.txt");
	a.readMatrix("ex.txt");
	//a.writeMatrix("ex2.txt");
	a.increaseRow();
	a.showMatrix();
	//a.transpose();
	return 0;
}