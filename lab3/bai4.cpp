#include <iostream>
#include <math.h>
using namespace std;

int factorial(int n)
{
	if (n < 2) {
		return 1;
	}
	else {
		return n*factorial(n - 1);
	}
}


float f1(float x, int n) {
	if (n == 1) {
		return x;
	}
	else {
		return pow(x, n) + f1(x, n - 1);
	}
}

float f3(float x, int n) {
	if (n == 0) {
		return 1;
	}
	else {
		return pow(x, n) / factorial(n) + f3(x, n - 1);
	}
}

float f(float x, int n, int i) {
	switch (i) {
	case 1:
		return f1(x, n) + n;
		break;
	case 2:
		return 0;
		break;
	case 3:
		return f3(x, n) + n;
		break;
	}
}

int main() {
	cout << f(2, 3, 1) << endl;
	return 0;
}