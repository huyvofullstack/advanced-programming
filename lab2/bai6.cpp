#include <iostream>
#include <fstream>
#include <ctype.h>
using namespace std;

int main() {
	char s[100];
	char ch;
	cout << "Nhap 1 chuoi bat ki: ";
	cin.getline(s, 100);
	fstream f("input.txt", ios::out);
	f << s;
	f.close();
	fstream inf("input.txt", ios::in);
	fstream outf("stdout.txt", ios::out);
	inf.get(ch);
	ch = toupper(ch);
	outf << ch;
	inf.get(ch);
	while (!inf.eof()) {
		if (ch == ' ') {
			outf << ch;
			inf.get(ch);
			ch = toupper(ch);
			outf << ch;
		}
		else {
			outf << ch;
		}
		inf.get(ch);
	}
	inf.close();
	outf.close();
	return 0;
}