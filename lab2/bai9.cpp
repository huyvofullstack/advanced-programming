#include <iostream>
#include <fstream>
using namespace std;

class Employee {
private:
	int ENO;
	char ENAME[10];
public:
	Employee() {
	}

	Employee(char x[10], int y) {
		this->ENO = y;
		this->ENAME[10] = x[10];
	}
	
	void GETIT() {
		cout << "ENAME: ";
		cin.getline(this->ENAME, 10);
		cout << "ENO: ";
		cin >> this->ENO;
	}
	
	void SHOWIT() {
		cout << this->ENO << " " << this->ENAME << endl;
	}

	void writeEmployee() {
		fstream f("EMPLOYEE.dat", ios::out);
		f << this->ENAME << endl;
		f << this->ENO << endl;
		f.close();
	}

	void showEmployee() {
		cout << "____Output from file.DAT:";
		char ch;
		fstream f("EMPLOYEE.dat", ios::in);
		f.get(ch);
		while (!f.eof()) {
			cout << ch;
			f.get(ch);
		}
		f.close();
	}

	void writeEmployeeDOC() {
		fstream f("EMPLOYEE.doc", ios::out);
		f << this->ENAME << endl;
		f << this->ENO << endl;
		f.close();
	}

	void showEmployeeDOC() {
		cout << "____Output from file.DOC:";
		char ch;
		fstream f("EMPLOYEE.doc", ios::in);
		f.get(ch);
		while (!f.eof()) {
			cout << ch;
			f.get(ch);
		}
		f.close();
	}
};

int main() {
	Employee x;
	x.GETIT();
	x.writeEmployee();
	x.showEmployee();
	x.writeEmployeeDOC();
	x.showEmployeeDOC();
	return 0;
}