#include <iostream>
#include <fstream>
using namespace std;

int main() {
	fstream inf("cad.txt", ios::in);
	fstream outf("cppad.txt", ios::out);
	char ch;
	inf.get(ch);
	while (!inf.eof()) {
		if (ch == 'C') {
			outf << "C++";
		}
		else outf.put(ch);
		inf.get(ch);
	}
	inf.close();
	outf.close();
	return 0;
}