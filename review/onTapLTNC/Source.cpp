#include <iostream>
#include <fstream>
using namespace std;

class Complex {
public:
	int _im;
	int _ireal;
public:
	Complex();
	Complex(int _im, int _ireal);
	~Complex();
	Complex operator / (const Complex& x);
	Complex operator - (const Complex& x);
	Complex operator = (const Complex& x);
	int readComplex(char* filename, Complex[], int &n);
	int writeComplex(char* filename, Complex[], int n);
	friend ostream& operator << (ostream& os, Complex x);
};

Complex::Complex() {
	this->_im = 0;
	this->_ireal = 0;
}

Complex::Complex(int _im, int _ireal) {
	this->_im = _im;
	this->_ireal = _ireal;
}

Complex::~Complex() {
	_im = NULL;
	_ireal = NULL;
}

Complex Complex::operator / (const Complex& x) {
	int imClone = (this->_im*x._im + this->_ireal*x._ireal) / (x._im*x._im + x._ireal*x._ireal);
	int irClone = (this->_ireal*x._im - this->_im*x._ireal) / (x._im*x._im + x._ireal*x._ireal);
	return Complex(imClone, irClone);
}

Complex Complex::operator - (const Complex& x) {
	int imClone = this->_im - x._im;
	int irClone = this->_ireal - x._ireal;
	return Complex(imClone, irClone);
}

Complex Complex::operator = (const Complex& x) {
	this->_im = x._im;
	this->_ireal = x._ireal;
	return *this;
}

ostream& operator << (ostream& os, Complex x) {
	os << x._im << '+' << x._ireal << endl;;
	return os;
}

int Complex::readComplex(char* filename, Complex c[], int& n) {
	fstream in(filename, ios::in);
	if (!in) {
		cout << "Khong mo duoc file" << endl;
		return 0;
	}
	while (!in.eof()) {
		in >> this->_im;
		in >> this->_ireal;
	}
	in.close();
	return 1;
}

int Complex::writeComplex(char* filename, Complex c[], int n) {
	fstream out(filename, ios::out);
	out << c[0]._im << endl;
	out << c[0]._ireal << endl;
	out.close();
	return 1;
}

int main(int argc, char* argv[]) {
	Complex c[100];
	Complex f;
	int n;
	f.readComplex(argv[1], c, n);
	f = (c[0] - c[1]);
	f = (c[0] - c[1]) / c[2];
	//cout << f << endl;
	f.writeComplex("output", c, n);
	
}