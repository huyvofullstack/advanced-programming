#include <iostream>
#include <fstream>
using namespace std;

class Node {
public:
	int data;
	Node* next;
	Node() {
		data = 0;
		next = NULL;
	}

	Node(int data) {
		this->data = data;
		this->next = NULL;
	}
};

class CList {
private:
	int count;
	Node* pHead;
public:
	CList() {
		count = 0;
		pHead = NULL;
	}

	~CList() {
		count = 0;
		Node*pTemp = this->pHead;
		while (pHead) {
			pHead = pHead->next;
			delete pTemp;
			pTemp = pHead;
		}
	}

	CList(const CList& x) {
		Node* pTemp2 = x.pHead;
		while (pTemp2) {
			Node* pTemp1 = new Node();
			pTemp1->data = pTemp2->data;
			if (this->pHead == NULL) {
				this->pHead = pTemp1;
			}
			else {
				Node* pPos = this->pHead;
				while (pPos->next != NULL) {
					pPos = pPos->next;
				}
				pPos->next = pTemp1;
			}
			pTemp2 = pTemp2->next;
		}
		this->count = x.count;
	}

	friend ostream& operator<<(ostream& os, const CList& x) {
		Node* pTemp = x.pHead;
		while (pTemp) {
			os << pTemp->data << " ";
			pTemp = pTemp->next;
		}
		return os;
	}

	friend istream& operator>>(istream& is, CList& x) {
		int data;
		while (!is.eof()) {
			is >> data;
			Node* pTemp = new Node(data);
			pTemp->next = x.pHead;
			x.pHead = pTemp;
			x.count++;
		}
		return is;
	}

	bool operator==(const CList& x) {
		int i = 0;
		Node* pTemp1 = this->pHead;
		Node* pTemp2 = x.pHead;
		while (pTemp1 && pTemp2) {
			if (pTemp1->data == pTemp2->data) {
				i++;
			}
			pTemp1 = pTemp1->next;
			pTemp2 = pTemp2->next;
		}
		if (i == this->count) {
			return true;
		}
		else return false;
	}

	CList& operator=(const CList& x) {
		if (this->pHead) {
			Node*pTemp = this->pHead;
			while (this->pHead) {
				this->pHead = this->pHead->next;
				delete pTemp;
				pTemp = this->pHead;
			}
		}
		Node* pTemp2 = x.pHead;
		while (pTemp2) {
			Node* pTemp1 = new Node();
			pTemp1->data = pTemp2->data;
			if (this->pHead == NULL) {
				this->pHead = pTemp1;
			}
			else {
				Node* pPos = this->pHead;
				while (pPos->next != NULL) {
					pPos = pPos->next;
				}
				pPos->next = pTemp1;
			}
			pTemp2 = pTemp2->next;
		}
		this->count = x.count;
		return *this;
	}

	CList operator+(const CList& x) {
		Node* pTemp2 = x.pHead;
		CList re = *this;
		re.count += x.count;
		Node* pTemp = re.pHead;
		while (pTemp->next) {
			pTemp = pTemp->next;
		}
		while (pTemp2) {
			pTemp->next = pTemp2;
			pTemp2 = pTemp2->next;
			pTemp = pTemp->next;
		}
		return re;
	}
};

int main() {
	CList a, b;
	fstream f1("a.txt", ios::in);
	f1 >> a;
	f1.close();
	fstream f2("b.txt", ios::in);
	f2 >> b;
	f2.close();
	fstream f3("outa.txt", ios::out);
	f3 << a;
	f3.close();
	fstream f4("outb.txt", ios::out);
	f4 << b;
	f4.close();
	cout << a << endl;
	cout << b << endl;
	cout << (a == b) << endl;
	CList c = a;
	cout << "c = " << c << endl;
	/*CList d = a + b;
	cout << "d = " << d << endl;*/
	return 0;
}