#include <iostream>
#include <fstream>
using namespace std;

class Node {
public:
	int data;
	Node* next;
	Node() {
		data = NULL;
		next = NULL;
	}

	Node(int data) {
		this->data = data;
	}
};

class CStack {
private:
	int count;
	Node* pTop;
public:
	CStack() {
		pTop = NULL;
		count = 0;
	}

	void pushStack(Node* &x, int data) {
		Node* pTemp = new Node(data);
		pTemp->next = x;
		x = pTemp;
	}

	void popStack(Node* &x, int& data) {
		Node*pTemp;
		if (!isEmpty(x))
		{
			pTemp = x;
			data = pTemp->data;
			x = x->next;
			delete pTemp;
		}
	}

	friend ostream& operator<<(ostream& os, const CStack& x) {
		Node*pTemp = x.pTop;
		while (pTemp != NULL) {
			os << pTemp->data << " ";
			pTemp = pTemp->next;
		}
		return os;
	}
	
	friend istream& operator>>(istream& is, CStack& x) {
		int data;
		while (!is.eof()) {
			is >> data;
			Node* pTemp = new Node(data);
			pTemp->next = x.pTop;
			x.pTop = pTemp;
			x.count++;
		}
		return is;
	}

	void moveStack(CStack &x) {
		int data;
		Node* pTemp = pTop;
		while (pTemp != NULL) {
			popStack(pTemp, data);
			pushStack(x.pTop, data);
		}
	}

	int isEmpty(Node* x) {
		return (x == NULL);
	}

	CStack& operator=(CStack &x) {
		CStack a;
		int data;
		this->count = x.count;
		while (x.pTop != NULL) {
			popStack(x.pTop, data);
			pushStack(a.pTop, data);
		}
		while (a.pTop != NULL) {
			popStack(a.pTop, data);
			pushStack(this->pTop, data);
			pushStack(x.pTop, data);
		}
		return *this;
	}

	int operator==(CStack &x) {
		CStack a;
		CStack b;
		int dem = 0, c = 0, d = 0;
		while (!this->isEmpty(this->pTop) && !x.isEmpty(x.pTop)) {
			popStack(this->pTop, c);
			popStack(x.pTop, d);
			if (c == d) {
				dem++;
			}
			pushStack(a.pTop, c);
			pushStack(b.pTop, d);
		}
		while (a.pTop != NULL) {
			popStack(a.pTop, c);
			pushStack(this->pTop, c);
		}
		b.moveStack(x);
		if (dem == this->count) {
			return 1;
		}
		else return 0;
	}

	CStack operator+(CStack &x) {
		CStack a;
		int data;
		this->count = x.count;
		while (x.pTop != NULL) {
			popStack(x.pTop, data);
			pushStack(a.pTop, data);
		}
		while (a.pTop != NULL) {
			popStack(a.pTop, data);
			pushStack(this->pTop, data);
			pushStack(x.pTop, data);
		}
		return *this;
	}
};

int main() {
	CStack x, y;
	fstream f1("x.txt", ios::in);
	f1 >> x;
	f1.close();
	fstream f2("y.txt", ios::in);
	f2 >> y;
	f2.close();
	cout << x << endl;
	cout << y << endl;
	cout << (x == y) << endl;
	CStack z = x;
	cout << z << endl;
	cout << x + y << endl;
	return 0;
}