#include <iostream>
#include <fstream>
using namespace std;

class Matrix {
private:
	int r;
	int c;
	int **a;
public:
	Matrix() {
		r = c = 0;
		a = NULL;
	}
	~Matrix() {
		if (this->a) {
			for (int i = 0; i < this->c; i++) {
				delete[] this->a[i];
			}
			delete[] this->a;
			this->a = NULL;
		}
	}

	friend ostream& operator<<(ostream& os, const Matrix& x) {
		for (int i = 0; i < x.r; i++) {
			for (int j = 0; i < x.c; i++) {
				os << x.a[i][j] << " ";
			}
			os << endl;
		}
		return os;
	}

	friend istream& operator>>(istream& is, Matrix& x) {
		is >> x.r;
		is >> x.c;
		x.a = new int*[x.r];
		for (int i = 0; i < x.r; i++) {
			x.a[i] = new int[x.c];
			for (int j = 0; i < x.c; i++) {
				is >> x.a[i][j];
			}
		}
		return is;
	}

	bool operator==(const Matrix& x) {
		int d = 0;
		for (int i = 0; i < x.r; i++) {
			for (int j = 0; i < x.c; i++) {
				if (this->a[i][j] == x.a[i][j]) {
					d++;
				}
			}
		}
		if (d == r*c) {
			return true;
		}
		else return false;
	}

	Matrix& operator=(const Matrix& x) {
		this->r = x.r;
		this->c = x.c;
		this->a = new int*[this->r];
		for (int i = 0; i < this->r; i++) {
			this->a[i] = new int[this->c];
			for (int j = 0; j < this->c; j++) {
				this->a[i][j] = x.a[i][j];
			}
		}
	}

	Matrix operator+(const Matrix& x) {
		Matrix re;
		re.c = this->c;
		re.r = this->r;
		re.a = new int*[this->r];
		for (int i = 0; i < this->r; i++) {
			re.a[i] = new int[this->c];
			for (int j = 0; j < this->c; j++) {
				re.a[i][j] = this->a[i][j] + re.[i][j];
			}
		}
		return re;
	}
};

int main() {
	Matrix a, b;
	fstream f1("a.txt", ios::in);
	f1 >> a;
	f1.close();
	fstream f2("b.txt", ios::in);
	f2 >> b;
	f2.close();
	cout << a << endl << endl << b << endl;
	/*cout << (a == b) << endl;
	Matrix c = a;*/
	return 0;
}