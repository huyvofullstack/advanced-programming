#include <iostream>
#include <fstream>
using namespace std;

class Complex {
private:
	double real;
	double imaginary;
public:
	Complex() {
		real = 0.00;
		imaginary = 0.00;
	}
	
	Complex(double real, double imaginary) {
		this->real = real;
		this->imaginary = imaginary;
	}

	friend ostream& operator<<(ostream& os, const Complex& x) {
		if (x.imaginary >= 0) {
			os << x.real << " + " << x.imaginary << "*i";
		}
		else {
			os << x.real << " - " << -x.imaginary << "*i";
		}
		return os;
	}

	friend istream& operator>>(istream& is, Complex& x) {
		cout << "Nhap phan thuc: ";
		is >> x.real;
		cout << "Nhap phan ao: ";
		is >> x.imaginary;
		return is;
	}

	Complex operator+(const Complex x) {
		double newReal, newImaginary;
		newReal = this->real + x.real;
		newImaginary = this->imaginary + x.imaginary;
		return Complex(newReal, newImaginary);
	}

	Complex operator-(const Complex x) {
		double newReal, newImaginary;
		newReal = this->real - x.real;
		newImaginary = this->imaginary - x.imaginary;
		return Complex(newReal, newImaginary);
	}

	Complex operator*(const Complex x) {
		double newReal, newImaginary;
		newReal = this->real * x.real - this->imaginary * x.imaginary;
		newImaginary = this->imaginary * x.real + this->real * x.imaginary;
		return Complex(newReal, newImaginary);
	}

	Complex operator/(const Complex x) {
		double newReal, newImaginary;
		newReal = (this->real * x.real + this->imaginary * x.imaginary) / (x.real * x.real + x.imaginary * x.imaginary);
		newImaginary = (this->imaginary * x.real - this->real * x.imaginary) / (x.real * x.real + x.imaginary * x.imaginary);
		return Complex(newReal, newImaginary);
	}

	void print() {
		if (this->imaginary >= 0) {
			cout << this->real << " + " << this->imaginary << "*i";
		}
		else {
			cout << this->real << " - " << this->imaginary << "*i";
		}
	}

};

int main() {
	Complex s1;
	Complex s2;
	fstream sum, sub, multi, div; 
	int question;
	char ch;
	cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
	cin >> ch;
	while (ch == 'Y' || ch == 'y') {
		cout << "Nhap cau hoi (1, 2, 3, 4, 5, 6, 7): ";
		cin >> question;
		switch (question) {
		case 1:
			ch = 'n';
			break;
		case 2:
			cin >> s1;
			cin >> s2;
			s1.print();
			cout << endl;
			s2.print();
			cout << endl;
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 3:
			cout << "So phuc 1: " << s1 << endl;
			cout << "So phuc 2: " << s2 << endl;
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 4:
			sum.open("sum.txt", ios::out);
			sum << '(' << s1 << ')' << " + " << '(' << s2 << ')' << " = " << '(' << s1 + s2 << ')';
			sum.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 5:
			sub.open("sub.txt", ios::out);
			sub << '(' << s1 << ')' << " - " << '(' << s2 << ')' << " = " << '(' << s1 - s2 << ')';
			sub.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 6:
			multi.open("multi.txt", ios::out);
			multi << '(' << s1 << ')' << " * " << '(' << s2 << ')' << " = " << '(' << s1 * s2 << ')';
			multi.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 7:
			div.open("div.txt", ios::out);
			div << '(' << s1 << ')' << " / " << '(' << s2 << ')' << " = " << '(' << s1 / s2 << ')';
			div.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		}
	}
	return 0;
}