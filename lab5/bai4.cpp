#include <iostream>
#include <fstream>
using namespace std;

class Array1D {
private:
	int n;
	int *a;
public:
	Array1D() {
		n = 0;
		a = NULL;
	}

	Array1D(Array1D& x) {
		this->n = x.n;
		this->a = new int[x.n];
		for (int i = 0; i < this->n; i++) {
			this->a[i] = x.a[i];
		}
	}

	~Array1D() {
		delete[] this->a;
		this->a = NULL;
	}

	friend ostream& operator<<(ostream& os, const Array1D& x) {
		for (int i = 0; i < x.n; i++) {
			os << x.a[i] << " ";
		}
		return os;
	}

	friend istream& operator>>(istream& is, Array1D& x) {
		is >> x.n;
		x.a = new int[x.n];
		for (int i = 0; i < x.n; i++) {
			is >> x.a[i];
		}
		return is;
	}

	Array1D operator+(const Array1D& x) {
		Array1D re;
		re.n = this->n;
		re.a = new int[this->n];
		for (int i = 0; i < this->n; i++) {
			re.a[i] = this->a[i] + x.a[i];
		}
		return re;
	}
	
	Array1D& operator=(const Array1D& x) {
		this->n = x.n;
		this->a = new int[x.n];
		for (int i = 0; i < this->n; i++) {
			this->a[i] = x.a[i];
		}
		return *this;
	}
	
	int operator==(const Array1D& x) {
		int d = 0;
		for (int i = 0; i < this->n; i++) {
			if (this->a[i] == x.a[i]) {
				d++;
			}
		}
		if (d == this->n) {
			return 1;
		}
		else return 0;
	}
};

int main() {
	Array1D x, y;
	fstream f1("A.txt", ios::in);
	f1 >> x;
	f1.close();
	fstream f2("B.txt", ios::in);
	f2 >> y;
	f2.close();
	Array1D z = x + y;
	cout << x << endl;
	cout << y << endl;
	cout << "x + y: " << z << endl;
	cout << (x == y) << endl;
	x = y;
	cout << x << endl;
	cout << (x == y) << endl;
	return 0;
}