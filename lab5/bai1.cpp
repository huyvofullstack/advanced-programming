#include <iostream>
#include <fstream>
using namespace std;

class PhanSo {
private:
	int tuSo;
	int mauSo;
public:
	PhanSo() {
		this->tuSo = 0;
		this->mauSo = 0;
	}

	PhanSo(int tuSo, int mauSo) {
		this->tuSo = tuSo;
		this->mauSo = mauSo;
	}

	friend ostream& operator<<(ostream& os, PhanSo &x) {
		os << x.tuSo << '/' << x.mauSo;
		return os;
	}

	friend istream& operator>>(istream& is, PhanSo &x) {
		cout << "Nhap tu so: ";
		is >> x.tuSo;
		cout << "Nhap mau so: ";
		is >> x.mauSo;
		return is;
	}
	 
	int UCLN(int a, int b) {
		int n = 0;
		while (b != 0) {
			n = a % b;
			a = b;
			b = n;
		}
		return a;
	}

	PhanSo operator+(const PhanSo x) {
		int a, b, c, d;
		if (this->mauSo == x.mauSo) {
			a = this->tuSo + x.tuSo;
			b = this->mauSo;
		}
		else {
			a = this->tuSo*x.mauSo + x.tuSo*this->mauSo;
			b = this->mauSo*x.mauSo;
		}
		c = a / UCLN(a, b);
		d = b / UCLN(a, b);
		return PhanSo(c, d);
	}

	PhanSo operator-(const PhanSo x) {
		int a, b, c, d;
		if (this->mauSo == x.mauSo) {
			a = this->tuSo - x.tuSo;
			b = this->mauSo;
		}
		else {
			a = this->tuSo*x.mauSo - x.tuSo*this->mauSo;
			b = this->mauSo*x.mauSo;
		}
		c = a / UCLN(a, b);
		d = b / UCLN(a, b);
		return PhanSo(c, d);
	}

	PhanSo operator*(const PhanSo x) {
		int a = this->tuSo*x.tuSo;
		int b = this->mauSo*x.mauSo;
		int c = a / UCLN(a, b);
		int d = b / UCLN(a, b);
		return PhanSo(c, d);
	}
};

int main() {
	PhanSo phanSo1;
	PhanSo phanSo2;
	fstream read1, read2, write1, write2, sum, sub, multi;
	int question;
	char ch;
	cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
	cin >> ch;
	while (ch == 'Y' || ch == 'y') {
		cout << "Nhap cau hoi (1, 2, 3, 4, 5, 6): ";
		cin >> question;
		switch (question) {
		case 1:
			ch = 'n';
			break;
		case 2:
			read1.open("ps1.txt", ios::in);
			read1 >> phanSo1;
			read1.close();
			cout << phanSo1 << endl;
			read2.open("ps2.txt", ios::in);
			read2 >> phanSo2;
			read2.close();
			cout << phanSo2 << endl;
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 3:
			write1.open("out_ps1.txt", ios::out);
			write1 << phanSo1;
			write1.close();
			write2.open("out_ps2.txt", ios::out);
			write2 << phanSo2;
			write2.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 4:
			sum.open("sum.txt", ios::out);
			sum << phanSo1 << " + " << phanSo2 << " = " << phanSo1 + phanSo2;
			sum.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 5:
			sub.open("sub.txt", ios::out);
			sub << phanSo1 << " - " << phanSo2 << " = " << phanSo1 - phanSo2;
			sub.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		case 6:
			multi.open("multi.txt", ios::out);
			multi << phanSo1 << " * " << phanSo2 << " = " << phanSo1 * phanSo2;
			multi.close();
			cout << "Ban co muon tiep tuc chuong trinh (Y or N): ";
			cin >> ch;
			break;
		}
	}
	return 0;
}